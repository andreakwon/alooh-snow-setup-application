# ALOOH SNOW Setup Application

## Description


본 안드로이드 App은 ALOOH SNOW 디바이스를 설정하기 위한 프로그램 입니다.

* 사용자 계정에 ALOOH SNOW 디바이스 등록
* 사용자 입력에 의해 ALOOH SNOW 디바이스 명을 ALOOH 클라우드 서버에 등록
* ALOOH SNOW 디바이스에 연결할 WiFi 네크워크(AP)를 설정